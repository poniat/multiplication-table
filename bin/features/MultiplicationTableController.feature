Feature: multiplication table
  In order to display the multiplication table
  In browser
  I need to provides size

Scenario: Display the multiplication table
  Given I am UNIX user and I call a script from CMD "true"
  And I am generating multiplication table size of 3 x 8
  Then I should get matrix of size 5 x 10
