# features/bootstrap/FeaturesContext.php
<?php

use Src\Controller\MultiplicationTableController;
use Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

class FeatureContext extends BehatContext
{
    private $matrix;

    /**
     * @Given /^I am UNIX user and I call a script from CMD "([^"]*)"$/
     */
    public function iAmUnixUserAndICallAScriptFromCmd(bool $arg1)
    {
        $this->matrix = new MultiplicationTableController();
        if($this->matrix->isCommandLineInterface() != $arg1) {
            throw new Exception("Access forbidden!");
        }
    }

    /**
     *  @Given /^I am generating multiplication table size of (\d+) x (\d+)$/
     */
    public function iAmGeneratingMultiplicationTableSizeOf(int $arg1, int $arg2)
    {
        $count = count($this->matrix->generateMatrix($arg1, $arg2));
        if ($count!= ++$arg1) {
            throw new Exception("Actual size of matrix: " . $count);
        }
    }

    /**
     * @Then /^I should get matrix of size (\d+) x (\d+)$/
     */
    public function iShouldGetMatrix(int $arg1, int $arg2)
    {
        $table = $this->matrix->generateCmdOutput($arg1, $arg2);
        if (!is_string($table) && strlen($table) != 587) {
            throw new Exception("Fail");
        }
    }


}