## Multiplication table 

####Project provides:
- Class which generate multiplication table of an arbitrary size
- CMD output of matrix
- PHPDoc
- PHPUnit tests
- Behat tests 
- Tested in W3C - HTML and CSS: 
**Document checking completed. No errors or warnings to show.**


#### An installation guide.
1. Create virtual host into your server and put directory index to path '/src/public/index.php'
2. Install a composer into main directory of the project. (`composer install`)
3. In order to test application run fallowing commands:
- `phpunit` - for PHPunit test,
- `bin/behat` - for Behat test.
4. In order to get matrix in CMD, please go to /src/public/ directory and run command: `php cmd.php rows columns`.

#### See working test below:
**[Test me live!](http://test.gcoders.co.uk/)**