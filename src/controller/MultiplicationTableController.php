<?php
declare(strict_types=1);

namespace Src\Controller;

class MultiplicationTableController
{
    /**
     * Max acceptable size of matrix
     */
    const MAX_SIZE = 20;

    /**
     * @var array storage of matrix
     */
    public $matrix = [];

    /**
     * Generate multiplication table of specific size
     *
     * @param int $rows number of rows
     * @param int $columns number of columns
     * @return array return array of matrix
     * @internal param int $size size fo matrix
     */
    public function generateMatrix(int $rows, int $columns) : array
    {
        $this->matrix = [];

        if($rows > 0 && $columns > 0)
        {
            $this->matrix[0][] = ['×', 'r'];
            for ($i=1; $i<=$columns; $i++) {
                $this->matrix[0][] = [$i, 'r'];
            }

            for ($i=1; $i<=$rows; $i++) {
                $this->matrix[$i][] = [$i, 'r'];
            }

            for ($i=1; $i<=$rows; $i++) {
                for ($j=1; $j<=$columns; $j++) {
                    $this->matrix[$i][$j] = [$i * $j, 'w'];
                }
            }
        }

        return $this->matrix;
    }

    /**
     * Check if call is coming from CMD or browser
     *
     * @return bool return true if call is CMD, otherwise false
     */
    public function isCommandLineInterface() : bool
    {
        return (php_sapi_name() === 'cli');
    }

    /**
     * Generate matrix in string type
     *
     * @param int $rows
     * @param int $columns
     * @param string $divider divider put between each digital
     * @return string return rendered matrix
     * @internal param int $size of the matrix
     */
    public function generateCmdOutput(int $rows, int $columns, string $divider = ' | ') : string
    {
        $output = '';
        if($this->isCommandLineInterface()) {
            $this->generateMatrix($rows, $columns);
            foreach ($this->matrix as $rows) {
                foreach ($rows as $cell) {
                    $cell = ($cell[1] == 'r') ? $this->getCLIBold((string)$cell[0]) : $cell[0];
                    $output .= $cell . $divider;
                }
                $output .= PHP_EOL;
            }
        }

        return $output;
    }

    public function getCLIBold(string $text) : string
    {
        return " \033[1m{$text}\033[0m ";
    }


}