<?php
require_once __DIR__ . '/../../vendor/autoload.php';

use Src\Controller\MultiplicationTableController;


if((isset($argv[1]) && $argv[1] > 0) && (isset($argv[2]) && $argv[2] > 0)) {
    $rows = (int) $argv[1];
    $columns = (int) $argv[2];

    $matrix = new MultiplicationTableController;
    echo $matrix->generateCmdOutput($rows, $columns);
}

die();