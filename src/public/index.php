<?php
require_once __DIR__ . '/../../vendor/autoload.php';

use Src\Controller\MultiplicationTableController;

$container = new \Slim\Container;
$container['settings']['displayErrorDetails'] = true;
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../views', [
        'cache' => false
    ]);
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', ''), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    return $view;
};

$app = new Slim\App();

$app->get('/', function ($request, $response) use($container){
    return $container['view']->render($response, 'index.html', []);
});

$app->post('/', function ($request, $response) use($container){
    $rows = $_POST['rows'];
    $columns = $_POST['columns'];

    $matrix = new MultiplicationTableController;
    return $container['view']->render($response, 'index.html', [
        "matrix" => $matrix->generateMatrix($rows, $columns)
    ]);
});

$app->run();
