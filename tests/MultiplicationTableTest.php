<?php

use Src\Controller\MultiplicationTableController;

class ConfigRepositoryTest extends \PHPUnit_Framework_TestCase
{
    public function test_matrix_values()
    {
        $size = 5;
        $matrix = new MultiplicationTableController;
        $table = $matrix->generateMatrix(5, 5);
        $temp = [
            0 => [ ['×', 'r'], [1, 'r'], [2, 'r'], [3, 'r'], [4, 'r'], [5, 'r']],
            1 => [ [1, 'r'],   [1, 'w'], [2,  'w'], [3,  'w'], [4,  'w'], [5,  'w']],
            2 => [ [2, 'r'],   [2, 'w'], [4,  'w'], [6,  'w'], [8,  'w'], [10, 'w']],
            3 => [ [3, 'r'],   [3, 'w'], [6,  'w'], [9,  'w'], [12, 'w'], [15, 'w']],
            4 => [ [4, 'r'],   [4, 'w'], [8,  'w'], [12, 'w'], [16, 'w'], [20, 'w']],
            5 => [ [5, 'r'],   [5, 'w'], [10, 'w'], [15, 'w'], [20, 'w'], [25, 'w']]
        ];

        $this->assertCount(++$size, $table);
        $this->assertEquals($table, $temp);
    }

    public function test_matrix_fail()
    {
        $matrix = new MultiplicationTableController;
        $table1 = $matrix->generateMatrix(0, 0);
        $table2 = $matrix->generateMatrix(-1, -2);

        $this->assertEquals($table1, []);
        $this->assertEquals($table2, []);
    }

    public function test_has_returns_true_if_script_is_run_via_cmd()
    {
        $matrix = new MultiplicationTableController;
        $cmd = $matrix->isCommandLineInterface();

        $this->assertTrue($cmd);
    }

    public function test_matrix_in_cmd()
    {
        $matrix = new MultiplicationTableController;
        $table = $matrix->generateCmdOutput(3, 6);

        $this->assertTrue(is_string($table));
    }


}